package ru.itis.gameapi.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.itis.gameapi.entity.Game;

import java.util.List;

@RestController
@RequestMapping("/game")
public class GameController {

    @GetMapping()
    public List<Game> getAllGames() {
        return List.of(
                Game.builder()
                        .name("The Witcher")
                        .genre("RPG")
                        .build(),
                Game.builder()
                        .name("CD MW3")
                        .genre("Shooter")
                        .build(),
                Game.builder()
                        .name("Hades")
                        .genre("Rogue-like")
                        .build()
        );
    }
}
