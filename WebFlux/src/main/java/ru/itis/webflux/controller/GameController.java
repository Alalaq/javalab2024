package ru.itis.webflux.controller;

import lombok.AllArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Flux;
import ru.itis.webflux.entity.Game;
import ru.itis.webflux.entity.Game;
import ru.itis.webflux.service.Service;

@AllArgsConstructor
@RestController
@RequestMapping("/game")
public class GameController {
    private final Service service;

    @GetMapping(produces = MediaType.TEXT_EVENT_STREAM_VALUE)
    public Flux<Game> getGames() {
        return service.getGames();
    }

}
