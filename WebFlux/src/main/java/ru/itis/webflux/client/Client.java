package ru.itis.webflux.client;

import reactor.core.publisher.Flux;
import ru.itis.webflux.entity.Game;

public interface Client {
    Flux<Game> getGames();
}
