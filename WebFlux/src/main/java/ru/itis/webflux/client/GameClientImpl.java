package ru.itis.webflux.client;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Flux;
import ru.itis.webflux.entity.Game;

import java.util.Arrays;

@Component
public class GameClientImpl implements Client {
    private final WebClient client;

    public GameClientImpl(@Value("${game.api.url}") String url) {
        client = WebClient.builder()
                .baseUrl(url)
                .build();
    }

    @Override
    public Flux<Game> getGames(){
        return client.get()
                .accept(MediaType.APPLICATION_JSON)
                .exchange()
                .flatMap(clientResponse -> clientResponse.bodyToMono(Game[].class))
                .flatMapIterable(Arrays::asList);
    }
}

