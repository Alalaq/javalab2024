package ru.itis.webflux.service;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Flux;
import reactor.core.scheduler.Schedulers;
import ru.itis.webflux.client.Client;
import ru.itis.webflux.entity.Game;

import java.util.List;

@Component
@AllArgsConstructor
public class GameServiceImpl implements Service {

    private final List<Client> clients;

    @Override
    public Flux<Game> getGames() {
        List<Flux<Game>> fluxes = clients.stream().map(this::getGames).toList();
        return Flux.merge((fluxes));
    }

    private Flux<Game> getGames(Client client) {
        return client.getGames()
                .subscribeOn(Schedulers.boundedElastic());
    }

}
