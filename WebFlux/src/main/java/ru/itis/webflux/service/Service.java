package ru.itis.webflux.service;

import reactor.core.publisher.Flux;
import ru.itis.webflux.entity.Game;
import ru.itis.webflux.entity.Game;

public interface Service {
    Flux<Game> getGames();
}
