package ru.itis.slowgameapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SlowGameApiApplication {

    public static void main(String[] args) {
        SpringApplication.run(SlowGameApiApplication.class, args);
    }

}
